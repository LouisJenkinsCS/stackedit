# CS Synch - Papers

##  Example - Paper Information

| Title | Conference | Abstract | Why is it important?
|------|----------------|-----------|-----------------|
| P == NP Solved! | PNP 2019 | '_Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum._'| Solves the millennium problem.

## Assignments

```mermaid
graph TD
Louis --> LouisAssigned[ICPP<br>ICDCS<br>ISMM<br>PLDI]
Wentao --> WentaoAssigned[NVMW<br>OOPSLA<br>EuroSys<br>EuroPar]
Hensen --> HensenAssigned[PPoPP<br>PODC<br>DISC<br>SPAA]
Mohammad --> MohammadAssigned[PACT<br>SOSP<br>OSDI<br>ASPLOS]
Alan --> AlanAssigned[???]
```

## Louis  - Papers

| Title | Conference | Abstract | Why is it important?
|------|----------------|-----------|-----------------|

## Wentao - Papers

| Title | Conference | Abstract | Why is it important?
|------|----------------|-----------|-----------------|

## Hensen - Papers

| Title | Conference | Abstract | Why is it important?
|------|----------------|-----------|-----------------|

## Mohammad - Papers

| Title | Conference | Abstract | Why is it important?
|------|----------------|-----------|-----------------|

## Alan - Papers

| Title | Conference | Abstract | Why is it important?
|------|----------------|-----------|-----------------|
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE0NjQ0MzMxOCw2NDg4OTc3OTJdfQ==
-->